#! /usr/bin/env python
# -*- coding: utf-8 -*-
# cassette.py
# Copyright 2011 Stefano Costa <steko@unisi.it>

import qrencode
import Image, ImageDraw, ImageFont

def qrlabel(n, txt):
    '''Creates a label with QR-Code and short text label.'''

    t = 'http://www.gortinabizantina.it/wiki/index.php/Cassetta_%d'
    s = 500
    n = n
    v, s1, im = qrencode.encode_scaled(t % n, s)

    box = (0, 0, s, s)
    region = im.crop(box)

    im2 = im.resize((s*2,s))
    im2.paste(255, (0,0,s*2,s))
    im2.paste(region, box)

    draw = ImageDraw.Draw(im2)
    font = ImageFont.truetype('Ubuntu-B.ttf', int(s/2.4))

    ts = draw.textsize(txt, font=font)
    d1x = int((s * 3 / 2 ) - ( ts[0] / 2 ))
    d1y = ( s / 4 ) - ( ts[1] / 2 )
    draw.text((d1x, d1y), txt, font=font)

    ts = draw.textsize('%d' % n, font=font)
    d1x = int((s * 3 / 2 ) - ( ts[0] / 2 ))
    d1y = ( s * 3 / 4 ) - ( ts[1] / 2 )
    draw.text((d1x, d1y), '%d' % n, font=font)

    return im2


if __name__ == '__main__':
    for n in range(200):
        with open('label%d.png' % n,'wb') as f:
            im = qrlabel(n,'GQB')
            im.save(f)
