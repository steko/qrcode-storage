======================
 Cassette con QR-Code
======================

:author: Stefano Costa

Esempi
======

Alcuni esempi dalla linea di comando::

    qrencode -o gqb12.png "http://www.gortinabizantina.it/wiki/Cassetta_12"
